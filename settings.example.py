
COINBASE = {
    'API_KEY': '',
    'API_SECRET': '',
}

BTCMARKETS = {
    'API_KEY': '',
    'API_SECRET': '',
}

QUOINE = {
    'API_ID': '',
    'API_SECRET': '',
}

PUSHBULLET_TOKENS = ['']

CHECK_INTERVAL = 2  # Seconds

MIN_PROFIT_SCALE = [
    {
        'limit_percent': 1,
        'min_profit_percent': 0.03,
        'buy_dollar_amount': 200
    },
    {
        'limit_percent': 0.8,
        'min_profit_percent': 0.03,
        'buy_dollar_amount': 200
    },
    {
        'limit_percent': 0.5,
        'min_profit_percent': 0.05,
        'buy_dollar_amount': 500
    },
    {
        'limit_percent': 0.2,
        'min_profit_percent': 0.1,
        'buy_dollar_amount': 1000
    },
    {
        'limit_percent': 0,
        'min_profit_percent': 0.1,
        'buy_dollar_amount': 2000
    },
]

HARD_MIN_SPEND = 100

ACTION_ONCE = False

TEST_MODE = False


# Logging setup
LOGGING = {
    'version': 1,
    'disable_existing_loggers': False,
    'formatters': {
        'standard': {
            'format': '%(asctime)s [%(levelname)s] %(name)s: %(message)s',
        },
        'csv': {
            'format': '%(asctime)s,%(message)s',
            'datefmt': '%Y/%m/%d %H:%M:%S'
        },
    },
    'handlers': {
        'default': {
            'level': 'INFO',
            'class': 'logging.handlers.RotatingFileHandler',
            'filename': 'logs/arbbot.log',
            'maxBytes': 1024*1024*5,  # 5MB
            'backupCount': 5,
            'formatter': 'standard'
        },
        'transaction': {
            'level': 'INFO',
            'class': 'logging.handlers.TimedRotatingFileHandler',
            'filename': 'logs/transactions/transactions.csv',
            'when': 'W0',
            'formatter': 'csv'
        },
        'tick': {
            'level': 'INFO',
            'class': 'logging.handlers.TimedRotatingFileHandler',
            'filename': 'logs/ticks/ticks.csv',
            'when': 'W0',
            'formatter': 'csv'
        },
        'price-watch': {
            'level': 'INFO',
            'class': 'logging.handlers.TimedRotatingFileHandler',
            'filename': 'logs/ticks/price-watch.csv',
            'when': 'W0',
            'formatter': 'csv'
        }
    },
    'loggers': {
        '': {
            'handlers': ['default'],
            'level': 'INFO',
            'propagate': True,
        },
        'transactions': {
            'handlers': ['transaction'],
            'level': 'INFO',
            'propagate': False,
        },
        'ticks': {
            'handlers': ['tick'],
            'level': 'INFO',
            'propagate': False,
        },
        'price-watch': {
            'handlers': ['price-watch'],
            'level': 'INFO',
            'propagate': False,
        },
        'urllib3.connectionpool': {
            'level': 'WARNING',
        }
    },
}
